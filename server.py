import sys
reload(sys)
sys.setdefaultencoding("UTF8")
import os
import uuid
from flask import *
from flask_socketio import SocketIO, emit
import psycopg2
import psycopg2.extras
import db as db
from cheapSharkWrapper import cheapSharkWrapper

app = Flask(__name__, static_folder='static')
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app)

@app.route('/', methods=['GET', 'POST'])
def index():
    return render_template('index.html')    
    
@app.route('/about', methods=['GET', 'POST'])
def about():
    return render_template('about.html')    

wishListQuery = 'SELECT * FROM wishList WHERE userEmail = %s'
@app.route('/wishlist', methods=['GET', 'POST'])
def wishlist():
    results = db.select_query_db(wishListQuery, [session['email']])
    return render_template('wishlist.html', wishList=results)    

@app.route('/gandalf', methods=['GET', 'POST'])
def gandalf():
    return render_template('gandalf.html')  

@app.route('/register', methods=['GET', 'POST'])
def registerPage():
    return render_template('register.html')  

'''@app.route('/search', namespace='/hack')
def searchTitle(title):
    api = a.cheapSharkWrapper("api")
    json = api.getDealByGameTitle(title)'''
    
doesUserAlreadyExist = 'SELECT * FROM users WHERE username = %s LIMIT 1'
registerNewUser = "INSERT INTO users VALUES (%s, %s, crypt(%s, gen_salt('bf')))"
@socketio.on('register', namespace='/hack')
def register(data):
    print data
    error = ''
    
    username = data['username']
    password = data['password']
    email = data['email']
    print("username: " + username)
    print("password: " + password)
    print("email: " + email)
    
    userExists = db.select_query_db(doesUserAlreadyExist, (username, ))
    
    if not userExists:
        db.write_query_db(registerNewUser, (email, username, password))
    
loginQuery = 'SELECT * from users WHERE (username = %s OR email = %s) AND password = crypt(%s, password)'
@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        
        username = request.form['username']
        pw = request.form['password']
        data = (username, username, pw)
        result = db.select_query_db(loginQuery, data, True)
        print result
        session['username'] = result['username']
        session['email'] = result['email']
        
        session['loggedin'] = True
        
    return render_template("index.html")
    
@app.route('/logout', methods=['GET', 'POST'])
def logout():
    session.clear()
    return redirect(url_for('index'))    

@socketio.on('loadTopDeals', namespace='/hack')
def loadDeals():
    api = cheapSharkWrapper("Loading Deals")
    results = api.getTopDeals(6)
    emit('loadTopDeals', results)

addGameQuery = 'INSERT INTO wishList (userEmail, game, price) VALUES (%s, %s, %s);'
@socketio.on('addGameToWishList', namespace='/hack')
def addGameToWishList(data):
    print data
    """
    if len(data['price']) == 0:
        data['price'] = 0.0
    """
    db.write_query_db(addGameQuery, [session['email'], data['game'], '0.0'] )

removeGameQuery = 'DELETE FROM wishList WHERE userEmail = %s AND game = %s';
@socketio.on('removeGameFromWishList', namespace='/hack')
def removeGameFromWishList(data):
    print data
    
    db.write_query_db(removeGameQuery, (session['email'], data['game']))

@app.route('/search', methods=['GET','POST'])
def searchForTitles():
    if request.method == "POST":
        searchTerm = request.form['searchBox']
        api = cheapSharkWrapper("Searching")
        searchResults = api.getDealByGameTitle(searchTerm)
        print(searchResults)
    return render_template("search.html",searchResults=searchResults)
    
# start the server
if __name__ == '__main__':
        socketio.run(app, host=os.getenv('IP', '0.0.0.0'), port =int(os.getenv('PORT', 8080)), debug=True)
