var HackyApp = angular.module('HackyApp', []);
var socket = io.connect('https://' + document.domain + ':' + location.port + '/hack');

HackyApp.controller('HackyController', function($scope){
    
    $scope.topDeals = [];

    $scope.game = '';

    $scope.register = function register(){
        var username = $('#username').val();
        var password = $('#password').val();
        var confirmPassword = $('#confirmPassword').val()
        var email = $('#email').val();
        var confirmEmail = $('#confirmEmail').val()
        
        console.log(username);
        console.log(password);
        console.log(confirmPassword);
        console.log(email);
        console.log(confirmEmail);
        
        if (username.length == 0 || password.length == 0 || confirmPassword.length == 0 || email.length == 0 || confirmEmail.length == 0) {
            return; //Error
        }
        
        socket.emit('register', {'username' : username, 'password' : password, 'email' : email});
        
    };
    
    $scope.loadLoginModal = function loadLoginModal(){
        $('#loginModal').modal('show');
    };
    
    $scope.loadTopDeals = function loadTopDeals(){
        socket.emit("loadTopDeals");
    };
    
    /*
    $scope.loadWishlist = function loadWishlist(){
        socket.emit("loadWishList");
    };
    */
    
    $scope.addGameToWishList = function addGameToWishList(game) {
        console.log(game + " added to wishList.");
        socket.emit('addGameToWishList', {'game': game});
    };
    
    $scope.removeGameFromWishList = function removeGameFromWishList(game) {
        console.log(game + " removed from wishList.");
        socket.emit('removeGameFromWishList', {'game': game});
    };
    
    $scope.remindMe = function remindMe(game) {
        console.log("Emailing reminder about " + game);
        //socket.emit('removeGameFromWishList', {'game': game});
    }; 
    socket.on('loadTopDeals', function(data){
        console.log(data)
         var list = [];
       for(var i = 0; i < data.length; i++){
           if(i % 3 == 0){
               list = [];
               $scope.topDeals.push(list);
           }
           data[i].savings = data[i].savings.split(".")[0]
           list.push(data[i])
       }
        console.log($scope.topDeals)
        $scope.$apply();
        
    });
    
    
    $( document ).ready(function() {
        console.log( "ready!" );
        $scope.loadTopDeals();
    });
    
});