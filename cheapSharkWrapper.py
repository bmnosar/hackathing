import urllib3
import requests

class cheapSharkWrapper:
    
    apiCall = """http://www.cheapshark.com/api/1.0/deals?"""
    
    def __init__(self, name):
        self.name = name
    
    def getOneDealByTitle(self, title):
        r = requests.get(cheapSharkWrapper.apiCall + "title=" + title + "&" + "pageSize=1")
        #print(r.data)
        return(r.json())
        
    def getTopDeals(self, numDeals):
        r = requests.get(cheapSharkWrapper.apiCall + "pageSize=" + str(numDeals))
        #print(r.data)
        return (r.json())
        
    def getDealByGameTitle(self,title):
        r = requests.get(cheapSharkWrapper.apiCall + "title=" + title)
        #print(r.data)
        return r.json()
    
    def getDealsByParameter(self, parameter):
        r = requests.get(cheapSharkWrapper.apiCall + parameter)
        #print(r.data)
        return r.json()
    
    def get20Deals(self, title):
        r = requests.get(cheapSharkWrapper.apiCall + "title=" +title+"&pageSize=20")
        return r.json()