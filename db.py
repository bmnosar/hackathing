import psycopg2

def connect_to_db():
    return psycopg2.connect('dbname=hackysack user=hackysack_normal password=password host=localhost')
    
def select_query_db(query, data=None, returnOne=False):
    
    db = connect_to_db()
    cur = db.cursor(cursor_factory=psycopg2.extras.DictCursor)
    results = []
    
    try:
        if data is None:
            mog = cur.mogrify(query)
        else:
            mog = cur.mogrify(query, data)
            
        cur.execute(mog)
        if returnOne == True:
            results = cur.fetchone()
        else:
            results = cur.fetchall()
        
    except Exception as e:
        print(e)
        db.rollback()
        
    cur.close()
    db.close()
    
    return results
    
def write_query_db(query, data, returnOne=False):
    
    db = connect_to_db()
    cur = db.cursor(cursor_factory=psycopg2.extras.DictCursor)
    
    results = []
    try:
        mog = cur.mogrify(query, data)
        cur.execute(mog)
        db.commit()
        if returnOne == True:
            results = cur.fetchone()
        
    except Exception as e:
        print(e)
        db.rollback()
        
    cur.close()
    db.close()
    
    if results:
        return results
   