DROP DATABASE IF EXISTS hackysack;
CREATE DATABASE hackysack ENCODING 'UTF8' TEMPLATE template0;
\c hackysack;

DROP ROLE IF EXISTS hackysack_normal;
DROP ROLE IF EXISTS hackysack_admin;

CREATE ROLE hackysack_normal WITH PASSWORD 'password' LOGIN;
CREATE ROLE hackysack_admin WITH PASSWORD 'password' LOGIN;

CREATE EXTENSION pgcrypto;

CREATE TABLE users(
    email text NOT NULL,
    username varchar(15),
    password text NOT NULL,
    PRIMARY KEY (email)
);

INSERT INTO users (email, username, password) VALUES ('bee@movie.com', 'bee', crypt('movie', gen_salt('bf')));

GRANT SELECT, INSERT, DELETE ON users TO hackysack_normal;

CREATE TABLE wishList (
    userEmail text NOT NULL REFERENCES users(email),
    game text,
    price decimal(6,2)
);

INSERT INTO wishList (userEmail, game, price) VALUES ('bee@movie.com', 'Halo: Combat Evolved', 5.00);

GRANT SELECT, INSERT, DELETE ON wishList TO hackysack_normal;